## [STEP 1] create project and sync with gitlab


## [STEP 2] change embeded server from tomcat to undertow

build gradle file 

change code

```
implementation 'org.springframework.boot:spring-boot-starter-undertow'

configurations {
    implementation {
        exclude group: 'org.springframework.boot', module: 'spring-boot-starter-tomcat'
    }
}
```

## [STEP 3] Undersdtand undertow and basic config

add package in main folder name 'config'

add java class under config folder name UndertowConfig.java

add @Configuration in UndertowConfig.java

create 
```
        @Bean

        UndertowServletWebServerFactory embededServletContainerFactory() {
                UndertowServletWebServerFactory factory = new UndertowServletWebServerFactory();
                factory.addBuilderCustomizers(
                        builder -> builder.setServerOption(UndertowOptions.ENABLE_HTTP2,true),
                        builder -> builder.setIoThreads(Runtime.getRuntime().availableProcessors() * 2),
                        builder -> builder.setWorkerThreads((Runtime.getRuntime().availableProcessors() * 10))
                );
                return factory;
        
        }
```
add server in application.properties

```
server.port=8181
server.compression.enabled=true
server.compression.min-response-size=1KB
server.http2.enabled=true

server.undertow.accesslog.enabled=true
server.undertow.accesslog.dir=undertow-accesslog
server.undertow.accesslog.pattern=%t %I %r %s %b %D

server.servlet.context-path=/santitee
```

